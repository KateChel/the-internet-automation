package alert;

import baseTest.BaseTest;
import enum_from_page.AlertsButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import paje_object.AlertsPage;
import paje_object.MainPage;

public class AlertTest extends BaseTest {

    AlertsPage alertsPage;

    //@BeforeClass
    @BeforeMethod
    public void beforeClass(){
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @Test
    public void alertTest(){
        //AlertsPage alertsPage = new AlertsPage(driver);
        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButton(AlertsButtons.ALERTS.getTextOnButton());
        Assert.assertEquals(alertsPage.switchToAlertAndGetText(true),"I am a JS Alert");
        Assert.assertEquals(alertsPage.getResultText(),"You successfully clicked an alert");
    }

    //--------------------------------------------------------------------------
    enum AlertButtons{
        /*ALERTS("JS Alert"),
        CONFIRM("JS Confirm"),
        PROMPT("JS Prompt");

        private String textOnButton;

        AlertButtons(String textOnButton) {
            this.textOnButton = textOnButton;
        }

        public String getTextOnButton() {
            return textOnButton;
        }

        @Override
        public String toString() {
            return textOnButton;
        }*/
    }

   /* public void clickOnButton(AlertButtons button){
        driver.findElement(By.xpath("//button[contains(text(),'"+ button.getTextOnButton() + "')]")).click();
    }*/

   /* public String switchToAlertAndGetText(boolean confirm, String...messages){
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        if (messages.length > 0) {
            alert.sendKeys(messages[0]);
        }
        if (confirm){
            alert.accept();
        }
        else {alert.dismiss();}
        return alertText;
    }

    public String getResultText(){
        return driver.findElement(By.cssSelector("#result")).getText();
    }*/
}
