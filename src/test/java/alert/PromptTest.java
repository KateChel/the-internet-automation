package alert;

import baseTest.BaseTest;
import enum_from_page.AlertsButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import paje_object.AlertsPage;
import paje_object.MainPage;

public class PromptTest extends BaseTest {
    AlertsPage alertsPage;
    @BeforeMethod
    public void beforeClass(){
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @DataProvider(name = "testParam")
    Object[] dataProvider(){
        return new Object[][]{
                {true, "Some test text!!!", "You entered: Some test text!!!"},
                {false, "Some test text!!!", "You entered: null"},
                {true, "", "You entered:"},
                {false, "", "You entered: null"}
        };

    }


    @Test(dataProvider="testParam")
    public void testPrompt(boolean switcher, String testText, String expectedText){
        //String testText = "Some test text from Kate";

        //AlertsPage alertsPage = new AlertsPage(driver);
        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");

        alertsPage.clickOnButton(AlertsButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(switcher, testText);
        Assert.assertEquals(alertsPage.getResultText(), expectedText);

        /*alertsPage.clickOnButton(AlertsButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(true, testText);
        Assert.assertEquals(alertsPage.getResultText(), "You entered: " + testText);

        alertsPage.clickOnButton(AlertsButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(false, testText);
        Assert.assertEquals(alertsPage.getResultText(), "You entered: null");

        alertsPage.clickOnButton(AlertsButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(true);
        Assert.assertEquals(alertsPage.getResultText(), "You entered:");

        alertsPage.clickOnButton(AlertsButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(false);
        Assert.assertEquals(alertsPage.getResultText(), "You entered: null");*/
    }
}
