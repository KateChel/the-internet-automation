package cookie;

import baseTest.BaseTest;
import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import paje_object.LoginFormPage;
import paje_object.MainPage;
import paje_object.SecurePage;

import java.util.Set;

public class TestCookie extends BaseTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;
    String userName = "tomsmith";
    String password = "SuperSecretPassword!";

    String expectedTooltipText = "You logged into a secure area!";

    SecurePage securePage;
    String expectedTitle = "Secure Area";
    String expectedErrorTooltipText = "You must login to view the secure area!";



    @BeforeClass
    public void beforeClass(){
        openUrl("https://the-internet.herokuapp.com\n");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
        securePage = new SecurePage(driver);
    }

    @Test
    public void cookieTest(){
        mainPage.clickOnBtnFromPage("login");
        loginFormPage.login(userName, password)
                .checkSuccessTooltip(expectedTooltipText);
        Set<Cookie> cookies = driver.manage().getCookies(); //создали коллекцию set с типом куки

        showCookies(cookies); //распечатываем куку
        System.out.println(); //распечатываем куку
        createCookies(cookies, "MyCookie", "MyValue");//добавляем куку
        showCookies(cookies); //распечатываем куку
        deleteMyCookies(cookies, "MyCookie", "MyValue");//удалить куку
        refresh();
        securePage.checkTitleOfPage(expectedTitle);
        deleteAllCookies();//удалить все куки
        refresh();
        Assert.assertEquals(loginFormPage.getMessageFromTooltip(false), expectedErrorTooltipText);
    }


}
