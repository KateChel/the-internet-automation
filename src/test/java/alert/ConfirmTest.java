package alert;

import baseTest.BaseTest;
import enum_from_page.AlertsButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import paje_object.AlertsPage;
import paje_object.MainPage;

public class ConfirmTest extends BaseTest {

    AlertsPage alertsPage;
    @BeforeMethod
    public void beforeClass(){
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @DataProvider(name = "test Param")
    Object[] dataProvider(){
        return new Object[][]{
            {true, "You clicked: Ok"},
            {false, "You clicked: Cancel"}
        };
    }

    @Test(dataProvider = "test Param")
    public void alertTest(boolean switcher, String expectedText) {
        //AlertsPage alertsPage = new AlertsPage(driver);
        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButton(AlertsButtons.CONFIRM.getTextOnButton());
        Assert.assertEquals(alertsPage.switchToAlertAndGetText(switcher),"I am a JS Confirm");
        Assert.assertEquals(alertsPage.getResultText(),expectedText);
    }
}
