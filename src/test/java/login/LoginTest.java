package login;

import baseTest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import paje_object.LoginFormPage;
import paje_object.MainPage;

public class LoginTest extends BaseTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;
    String userName = "tomsmith";
    String password = "SuperSecretPassword!";

    String expectedTooltipText = "You logged into a secure area!";


    @BeforeClass
    public void beforeClass(){
        mainPage = new MainPage(driver);
        openUrl("https://the-internet.herokuapp.com/");
        loginFormPage = new LoginFormPage(driver);
    }


    @Test
    public void successfulLoginTest(){
        mainPage.clickOnBtnFromPage("login");
        loginFormPage.login(userName, password)
                .checkSuccessTooltip(expectedTooltipText);
    //public void loginTest(){
       /*
       driver.findElement(By.xpath("//a[@href='/login']")).click();
        //Assert.assertEquals(driver.getCurrentUrl("https://the-internet.herokuapp.com/login"));
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
        driver.findElement(By.cssSelector("#username")).sendKeys("tomsmith");
        driver.findElement(By.cssSelector("#password")).sendKeys("SuperSecretPassword!");
        //driver.findElement(By.cssSelector("#password")).sendKeys();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//h2")).isDisplayed());
        Assert.assertEquals(driver.findElement(By.xpath("//h2")).getText(), "Secure Area");
        */



        /*driver.findElement(By.xpath("//input[@type='text']")).click(); //Username
        driver.findElement(By.xpath("//input[@type='password']")).click(); //password
        driver.findElement(By.xpath("//button[@class='radius']")).click(); //button Login
        //button[@type='submit']   //button Login
        */

    }
}
