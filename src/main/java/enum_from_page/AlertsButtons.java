package enum_from_page;

public enum AlertsButtons {
    ALERTS("JS Alert"),
    CONFIRM("JS Confirm"),
    PROMPT("JS Prompt");

    private String textOnButton;

    AlertsButtons(String textOnButton) {
        this.textOnButton = textOnButton;
    }

    public String getTextOnButton() {
        return textOnButton;
    }

    @Override
    public String toString() {
        return textOnButton;
    }
}
