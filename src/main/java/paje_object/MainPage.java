package paje_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage {
    public MainPage(WebDriver driver) {
        super(driver);
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    /*@FindBy(xpath = "//a[@href='/javascript_alerts']")
    private WebElement AlertsBtn;*/

   // private By alerBtn1 = By.xpath("//a[@href='/javascript_alerts']");

    /*private By alerBtn1(){
        return By.xpath("//a[@href='/javascript_alerts']");
    }*/

    private By btnFromPage(String nameButton){
        return By.xpath("//a[@href='/" + nameButton + "']");
    }
    //-----------------------------------
    public void clickOnBtnFromPage(String nameButton){
        driver.findElement(btnFromPage(nameButton)).click();
    }
}