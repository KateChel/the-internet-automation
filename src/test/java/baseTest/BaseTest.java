package baseTest;

//import driver.WebDriverFactory;
import driver.WebDriverFactory;
//import org.driver.WebDriverFactory;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.Set;

public class BaseTest {

    protected WebDriver driver = null;

    @BeforeTest
    public void setUpBrowser(){
        /*ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();*/
        driver = WebDriverFactory.initDriver();//Browsers.FIREFOX
        driver.manage().window().maximize();
    }

    @AfterTest
    public void closeDriver(){
        if (driver != null){
            driver.quit();
        }
    }

    public void openUrl(String url){
        driver.navigate().to(url);
        driver.manage().window().maximize();
    }

    public void showCookies(Set<Cookie> cookies){
        System.out.println();
        for(Cookie cookie : cookies){
            System.out.println(cookie.getName() + "|||" + cookie.getValue() + "|||" + cookie.getExpiry());
        }
    }

    public void createCookies(Set<Cookie> cookies, String cookieName,String cookieValue){
        //Cookie myCookie = new Cookie("MyCookie", "MyValue");
        //driver.manage().addCookie(myCookie);
        //driver.manage().addCookie(new Cookie("MyCookie", "MyValue"));
        driver.manage().addCookie(new Cookie(cookieName, cookieValue));
        cookies = driver.manage().getCookies();
        for(Cookie cookie : cookies){
            System.out.println(cookie.getName() + "|||" + cookie.getValue() + "|||" + cookie.getExpiry());
        }
    }

    public void deleteMyCookies(Set<Cookie> cookies, String cookieName,String cookieValue){
        //driver.manage().deleteCookieNamed(myCookie.getName());
        driver.manage().deleteCookieNamed(cookieName);
        cookies = driver.manage().getCookies();
    }

    public void deleteAllCookies(){
        driver.manage().deleteAllCookies();
    }

    public void refresh(){
        driver.navigate().refresh();
    }

}
